class CreateReportResults < ActiveRecord::Migration
  def change
    create_table :report_results do |t|
      t.integer :report_id
      t.integer :server_id
      t.text :result

      t.timestamps
    end
  end
end
