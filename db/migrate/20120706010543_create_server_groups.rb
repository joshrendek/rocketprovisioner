class CreateServerGroups < ActiveRecord::Migration
  def change
    create_table :server_groups do |t|
      t.string :name

      t.timestamps
    end
  end
end
