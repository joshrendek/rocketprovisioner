class AddPayloadTypeToPayloads < ActiveRecord::Migration
  def change
    add_column :payloads, :payload_type, :string, :default => "script"
  end
end
