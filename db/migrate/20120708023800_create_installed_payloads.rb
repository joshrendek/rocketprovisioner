class CreateInstalledPayloads < ActiveRecord::Migration
  def change
    create_table :installed_payloads do |t|
      t.integer :payload_id
      t.integer :server_id

      t.timestamps
    end
  end
end
