class CreateServers < ActiveRecord::Migration
  def change
    create_table :servers do |t|
      t.string :dns_name
      t.string :ip_address
      t.integer :server_group_id
      t.integer :server_role_id

      t.timestamps
    end
  end
end
