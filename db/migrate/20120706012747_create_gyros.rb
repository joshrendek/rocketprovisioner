class CreateGyros < ActiveRecord::Migration
  def change
    create_table :gyros do |t|
      t.string :name

      t.timestamps
    end
  end
end
