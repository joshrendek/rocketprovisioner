class AddReportInstanceIdToReportResults < ActiveRecord::Migration
  def change
    add_column :report_results, :report_instance_id, :integer
  end
end
