class AddFinishedToServers < ActiveRecord::Migration
  def change
    add_column :payload_histories, :finished, :bool, :default => false
  end
end
