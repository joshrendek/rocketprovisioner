class AddSshUserToServers < ActiveRecord::Migration
  def change
    add_column :servers, :ssh_user, :string
  end
end
