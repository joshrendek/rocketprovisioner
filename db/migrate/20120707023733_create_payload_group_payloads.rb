class CreatePayloadGroupPayloads < ActiveRecord::Migration
  def change
    create_table :payload_group_payloads do |t|
      t.integer :payload_id
      t.integer :payload_group_id

      t.timestamps
    end
  end
end
