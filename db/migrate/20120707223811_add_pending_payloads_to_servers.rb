class AddPendingPayloadsToServers < ActiveRecord::Migration
  def change
    add_column :servers, :pending_payloads, :integer, :default => 0
  end
end
