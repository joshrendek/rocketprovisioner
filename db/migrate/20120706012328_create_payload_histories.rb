class CreatePayloadHistories < ActiveRecord::Migration
  def change
    create_table :payload_histories do |t|
      t.integer :server_id
      t.integer :payload_id
      t.text :output
      t.integer :return_code

      t.timestamps
    end
  end
end
