class CreateRocketFiles < ActiveRecord::Migration
  def change
    create_table :rocket_files do |t|
      t.string :name
      t.text :content
      t.integer :folder_id

      t.timestamps
    end
  end
end
