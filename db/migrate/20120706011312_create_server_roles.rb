class CreateServerRoles < ActiveRecord::Migration
  def change
    create_table :server_roles do |t|
      t.string :name

      t.timestamps
    end
  end
end
