class CreatePayloadGroups < ActiveRecord::Migration
  def change
    create_table :payload_groups do |t|
      t.string :name

      t.timestamps
    end
  end
end
