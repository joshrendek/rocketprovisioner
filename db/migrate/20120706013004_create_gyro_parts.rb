class CreateGyroParts < ActiveRecord::Migration
  def change
    create_table :gyro_parts do |t|
      t.integer :gyro_id

      t.timestamps
    end
  end
end
