# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120723001418) do

  create_table "folders", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "gyro_parts", :force => true do |t|
    t.integer  "gyro_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "gyros", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "installed_payloads", :force => true do |t|
    t.integer  "payload_id"
    t.integer  "server_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "payload_group_payloads", :force => true do |t|
    t.integer  "payload_id"
    t.integer  "payload_group_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "payload_groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "payload_histories", :force => true do |t|
    t.integer  "server_id"
    t.integer  "payload_id"
    t.text     "output"
    t.integer  "return_code"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.boolean  "finished",    :default => false
  end

  create_table "payloads", :force => true do |t|
    t.string   "name"
    t.text     "script"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.string   "payload_type", :default => "script"
  end

  create_table "report_instances", :force => true do |t|
    t.integer  "report_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "report_results", :force => true do |t|
    t.integer  "report_id"
    t.integer  "server_id"
    t.text     "result"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "report_instance_id"
  end

  create_table "reports", :force => true do |t|
    t.string   "name"
    t.integer  "payload_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "rocket_files", :force => true do |t|
    t.string   "name"
    t.text     "content"
    t.integer  "folder_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "server_groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "server_roles", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "servers", :force => true do |t|
    t.string   "dns_name"
    t.string   "ip_address"
    t.integer  "server_group_id"
    t.integer  "server_role_id"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.string   "ssh_user"
    t.integer  "pending_payloads", :default => 0
  end

end
