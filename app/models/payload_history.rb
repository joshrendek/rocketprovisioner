class PayloadHistory < ActiveRecord::Base
  attr_accessible :output, :payload_id, :return_code, :server_id, :finished
  belongs_to :payload 
  belongs_to :server

  default_scope order('id desc')
  scope :pending, where(:finished => false)
end
