class Server < ActiveRecord::Base
  attr_accessible :dns_name, :ip_address, :server_group_id, :server_role_id, :ssh_user
  validates_presence_of :server_group_id, :dns_name, :ip_address
  belongs_to :server_group
  belongs_to :server_role

  has_many :payload_history

  def payload_installed?(payload)
    InstalledPayload.where(:server_id => self.id, :payload_id => payload.id).count > 0 ? true : false
  end

  def to_s 
    "#{dns_name} (#{ip_address})"
  end

end
