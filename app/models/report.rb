class Report < ActiveRecord::Base
  attr_accessible :name, :payload_id
  has_many :report_instances
  belongs_to :payload
end
