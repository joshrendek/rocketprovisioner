class ServerGroup < ActiveRecord::Base
  attr_accessible :name
  has_many :servers

  def to_s 
    name
  end
end
