class Payload < ActiveRecord::Base
  attr_accessible :name, :script, :payload_type
  belongs_to :payload_group_payloads
  scope :scripts, where(:payload_type => 'script')
  scope :packages, where(:payload_type => 'package')
  def to_s 
    name
  end
end
