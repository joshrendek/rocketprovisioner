class PayloadGroup < ActiveRecord::Base
  attr_accessible :name
  has_many :payload_group_payloads
  has_many :payloads, :through => :payload_group_payloads
  validates_presence_of :name
end
