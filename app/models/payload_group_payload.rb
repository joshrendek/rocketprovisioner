class PayloadGroupPayload < ActiveRecord::Base
  belongs_to :payload
  belongs_to :payload_group
  attr_accessible :payload_group_id, :payload_id
end
