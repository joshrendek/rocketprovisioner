class ReportInstance < ActiveRecord::Base
  attr_accessible :report_id
  belongs_to :report 
  has_many :report_results
end
