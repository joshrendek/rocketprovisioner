class ReportResult < ActiveRecord::Base
  attr_accessible :report_id, :result, :server_id, :report_instance_id
  belongs_to :report 
  belongs_to :report_instance
  belongs_to :server
end
