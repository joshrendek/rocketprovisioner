module PayloadsHelper
  def payload_linkable(txt, obj,  payload, action = nil, css) 
    if obj.is_a?(Server)
      link_to txt, run_payload_server_path(obj.id, payload.id, :package_action => action), :class => css, :remote => true
    elsif obj.is_a?(ServerGroup)
      link_to txt, run_payload_payload_group_path(obj.id, payload.id, :package_action => action), :class => css, :remote => true
    end
  end
end
