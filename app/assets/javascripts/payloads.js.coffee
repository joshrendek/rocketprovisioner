# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
#
$('#payload_payload_type').live 'change', (event) ->
  if $('#payload_payload_type').val() == 'package'
      $('#payload_script').remove()
      $('.pl_type_area .controls').html('<input type="text" class="text optional" name="payload[script]">')
      $('.pl_type_area .pl_type_label').html('Package(s)')
  else
      $('.pl_type_area .controls').html('<textarea class="text optional" cols="40" id="payload_script" name="payload[script]" rows="20">')
      $('.pl_type_area .pl_type_label').html('Script')

@payload_history = (div_id, server_id) ->
    $(div_id).load('/servers/' + server_id + '/payload_histories')
    setTimeout(->
        payload_history(div_id, server_id)
    , 3000)
