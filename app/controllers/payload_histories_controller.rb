class PayloadHistoriesController < ApplicationController
  layout false
  # GET /payload_histories
  # GET /payload_histories.json
  def index
    @server = Server.find(params[:server_id])
    @payload_histories = PayloadHistory.all
    render '_history'
  end

  # GET /payload_histories/1
  # GET /payload_histories/1.json
  def show
    @payload_history = PayloadHistory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @payload_history }
    end
  end

  # GET /payload_histories/new
  # GET /payload_histories/new.json
  def new
    @payload_history = PayloadHistory.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @payload_history }
    end
  end

  # GET /payload_histories/1/edit
  def edit
    @payload_history = PayloadHistory.find(params[:id])
  end

  # POST /payload_histories
  # POST /payload_histories.json
  def create
    @payload_history = PayloadHistory.new(params[:payload_history])

    respond_to do |format|
      if @payload_history.save
        format.html { redirect_to @payload_history, notice: 'Payload history was successfully created.' }
        format.json { render json: @payload_history, status: :created, location: @payload_history }
      else
        format.html { render action: "new" }
        format.json { render json: @payload_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /payload_histories/1
  # PUT /payload_histories/1.json
  def update
    @payload_history = PayloadHistory.find(params[:id])

    respond_to do |format|
      if @payload_history.update_attributes(params[:payload_history])
        format.html { redirect_to @payload_history, notice: 'Payload history was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @payload_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payload_histories/1
  # DELETE /payload_histories/1.json
  def destroy
    @payload_history = PayloadHistory.find(params[:id])
    @payload_history.destroy

    respond_to do |format|
      format.html { redirect_to payload_histories_url }
      format.json { head :no_content }
    end
  end
end
