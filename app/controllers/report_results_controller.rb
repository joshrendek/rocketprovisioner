class ReportResultsController < ApplicationController
  # GET /report_results
  # GET /report_results.json
  def index
    @report_results = ReportResult.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @report_results }
    end
  end

  # GET /report_results/1
  # GET /report_results/1.json
  def show
    @report_result = ReportResult.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @report_result }
    end
  end

  # GET /report_results/new
  # GET /report_results/new.json
  def new
    @report_result = ReportResult.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @report_result }
    end
  end

  # GET /report_results/1/edit
  def edit
    @report_result = ReportResult.find(params[:id])
  end

  # POST /report_results
  # POST /report_results.json
  def create
    @report_result = ReportResult.new(params[:report_result])

    respond_to do |format|
      if @report_result.save
        format.html { redirect_to @report_result, notice: 'Report result was successfully created.' }
        format.json { render json: @report_result, status: :created, location: @report_result }
      else
        format.html { render action: "new" }
        format.json { render json: @report_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /report_results/1
  # PUT /report_results/1.json
  def update
    @report_result = ReportResult.find(params[:id])

    respond_to do |format|
      if @report_result.update_attributes(params[:report_result])
        format.html { redirect_to @report_result, notice: 'Report result was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @report_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /report_results/1
  # DELETE /report_results/1.json
  def destroy
    @report_result = ReportResult.find(params[:id])
    @report_result.destroy

    respond_to do |format|
      format.html { redirect_to report_results_url }
      format.json { head :no_content }
    end
  end
end
