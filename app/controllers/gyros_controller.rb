class GyrosController < ApplicationController
  # GET /gyros
  # GET /gyros.json
  def index
    @gyros = Gyro.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @gyros }
    end
  end

  # GET /gyros/1
  # GET /gyros/1.json
  def show
    @gyro = Gyro.find(params[:id])
    @gyro_parts = @gyro.gyro_parts

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @gyro }
    end
  end

  # GET /gyros/new
  # GET /gyros/new.json
  def new
    @gyro = Gyro.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @gyro }
    end
  end

  # GET /gyros/1/edit
  def edit
    @gyro = Gyro.find(params[:id])
  end

  # POST /gyros
  # POST /gyros.json
  def create
    @gyro = Gyro.new(params[:gyro])

    respond_to do |format|
      if @gyro.save
        format.html { redirect_to @gyro, notice: 'Gyro was successfully created.' }
        format.json { render json: @gyro, status: :created, location: @gyro }
      else
        format.html { render action: "new" }
        format.json { render json: @gyro.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /gyros/1
  # PUT /gyros/1.json
  def update
    @gyro = Gyro.find(params[:id])

    respond_to do |format|
      if @gyro.update_attributes(params[:gyro])
        format.html { redirect_to @gyro, notice: 'Gyro was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @gyro.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gyros/1
  # DELETE /gyros/1.json
  def destroy
    @gyro = Gyro.find(params[:id])
    @gyro.destroy

    respond_to do |format|
      format.html { redirect_to gyros_url }
      format.json { head :no_content }
    end
  end
end
