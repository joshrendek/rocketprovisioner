class PayloadGroupsController < ApplicationController

  def add_payload
    PayloadGroupPayload.create(:payload_id => params[:pg][:payload_id], :payload_group_id => params[:id])
    redirect_to payload_group_path(params[:id]), notice: "Payload added to group."
  end

  def index
    @payload_groups = PayloadGroup.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @payload_groups }
    end
  end

  def show
    @payload_group = PayloadGroup.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @payload_group }
    end
  end

  # GET /payload_groups/new
  # GET /payload_groups/new.json
  def new
    @payload_group = PayloadGroup.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @payload_group }
    end
  end

  # GET /payload_groups/1/edit
  def edit
    @payload_group = PayloadGroup.find(params[:id])
  end

  # POST /payload_groups
  # POST /payload_groups.json
  def create
    @payload_group = PayloadGroup.new(params[:payload_group])

    respond_to do |format|
      if @payload_group.save
        format.html { redirect_to @payload_group, notice: 'Payload group was successfully created.' }
        format.json { render json: @payload_group, status: :created, location: @payload_group }
      else
        format.html { render action: "new" }
        format.json { render json: @payload_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /payload_groups/1
  # PUT /payload_groups/1.json
  def update
    @payload_group = PayloadGroup.find(params[:id])

    respond_to do |format|
      if @payload_group.update_attributes(params[:payload_group])
        format.html { redirect_to @payload_group, notice: 'Payload group was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @payload_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payload_groups/1
  # DELETE /payload_groups/1.json
  def destroy
    @payload_group = PayloadGroup.find(params[:id])
    @payload_group.destroy

    respond_to do |format|
      format.html { redirect_to payload_groups_url }
      format.json { head :no_content }
    end
  end
end
