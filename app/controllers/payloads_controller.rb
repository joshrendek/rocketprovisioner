class PayloadsController < ApplicationController
  # GET /payloads
  # GET /payloads.json
  def index
    @payloads = Payload.all
    @scripts = Payload.scripts 
    @packages = Payload.packages

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @payloads }
    end
  end

  # GET /payloads/1
  # GET /payloads/1.json
  def show
    @payload = Payload.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @payload }
    end
  end

  # GET /payloads/new
  # GET /payloads/new.json
  def new
    @payload = Payload.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @payload }
    end
  end

  # GET /payloads/1/edit
  def edit
    @payload = Payload.find(params[:id])
  end

  # POST /payloads
  # POST /payloads.json
  def create
    @payload = Payload.new(params[:payload])

    respond_to do |format|
      if @payload.save
        format.html { redirect_to @payload, notice: 'Payload was successfully created.' }
        format.json { render json: @payload, status: :created, location: @payload }
      else
        format.html { render action: "new" }
        format.json { render json: @payload.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /payloads/1
  # PUT /payloads/1.json
  def update
    @payload = Payload.find(params[:id])

    respond_to do |format|
      if @payload.update_attributes(params[:payload])
        format.html { redirect_to @payload, notice: 'Payload was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @payload.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payloads/1
  # DELETE /payloads/1.json
  def destroy
    @payload = Payload.find(params[:id])
    @payload.destroy

    respond_to do |format|
      format.html { redirect_to payloads_url }
      format.json { head :no_content }
    end
  end
end
