class RocketFilesController < ApplicationController
  # GET /rocket_files
  # GET /rocket_files.json
  def index
    @rocket_files = RocketFile.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @rocket_files }
    end
  end

  # GET /rocket_files/1
  # GET /rocket_files/1.json
  def show
    @rocket_file = RocketFile.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @rocket_file }
    end
  end

  # GET /rocket_files/new
  # GET /rocket_files/new.json
  def new
    @rocket_file = RocketFile.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @rocket_file }
    end
  end

  # GET /rocket_files/1/edit
  def edit
    @rocket_file = RocketFile.find(params[:id])
  end

  # POST /rocket_files
  # POST /rocket_files.json
  def create
    @rocket_file = RocketFile.new(params[:rocket_file])

    respond_to do |format|
      if @rocket_file.save
        format.html { redirect_to @rocket_file, notice: 'Rocket file was successfully created.' }
        format.json { render json: @rocket_file, status: :created, location: @rocket_file }
      else
        format.html { render action: "new" }
        format.json { render json: @rocket_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /rocket_files/1
  # PUT /rocket_files/1.json
  def update
    @rocket_file = RocketFile.find(params[:id])

    respond_to do |format|
      if @rocket_file.update_attributes(params[:rocket_file])
        format.html { redirect_to @rocket_file, notice: 'Rocket file was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @rocket_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rocket_files/1
  # DELETE /rocket_files/1.json
  def destroy
    @rocket_file = RocketFile.find(params[:id])
    @rocket_file.destroy

    respond_to do |format|
      format.html { redirect_to rocket_files_url }
      format.json { head :no_content }
    end
  end
end
