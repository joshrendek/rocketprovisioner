class GyroPartsController < ApplicationController
  # GET /gyro_parts
  # GET /gyro_parts.json
  def index
    @gyro_parts = GyroPart.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @gyro_parts }
    end
  end

  # GET /gyro_parts/1
  # GET /gyro_parts/1.json
  def show
    @gyro_part = GyroPart.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @gyro_part }
    end
  end

  # GET /gyro_parts/new
  # GET /gyro_parts/new.json
  def new
    @gyro_part = GyroPart.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @gyro_part }
    end
  end

  # GET /gyro_parts/1/edit
  def edit
    @gyro_part = GyroPart.find(params[:id])
  end

  # POST /gyro_parts
  # POST /gyro_parts.json
  def create
    @gyro_part = GyroPart.new(params[:gyro_part])

    respond_to do |format|
      if @gyro_part.save
        format.html { redirect_to @gyro_part, notice: 'Gyro part was successfully created.' }
        format.json { render json: @gyro_part, status: :created, location: @gyro_part }
      else
        format.html { render action: "new" }
        format.json { render json: @gyro_part.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /gyro_parts/1
  # PUT /gyro_parts/1.json
  def update
    @gyro_part = GyroPart.find(params[:id])

    respond_to do |format|
      if @gyro_part.update_attributes(params[:gyro_part])
        format.html { redirect_to @gyro_part, notice: 'Gyro part was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @gyro_part.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gyro_parts/1
  # DELETE /gyro_parts/1.json
  def destroy
    @gyro_part = GyroPart.find(params[:id])
    @gyro_part.destroy

    respond_to do |format|
      format.html { redirect_to gyro_parts_url }
      format.json { head :no_content }
    end
  end
end
