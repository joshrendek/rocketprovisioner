class ReportInstancesController < ApplicationController
  def show
    @report_instance = ReportInstance.find(params[:id])
    @report = Report.find(params[:report_id])
  end
end
