require "spec_helper"

describe ReportResultsController do
  describe "routing" do

    it "routes to #index" do
      get("/report_results").should route_to("report_results#index")
    end

    it "routes to #new" do
      get("/report_results/new").should route_to("report_results#new")
    end

    it "routes to #show" do
      get("/report_results/1").should route_to("report_results#show", :id => "1")
    end

    it "routes to #edit" do
      get("/report_results/1/edit").should route_to("report_results#edit", :id => "1")
    end

    it "routes to #create" do
      post("/report_results").should route_to("report_results#create")
    end

    it "routes to #update" do
      put("/report_results/1").should route_to("report_results#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/report_results/1").should route_to("report_results#destroy", :id => "1")
    end

  end
end
