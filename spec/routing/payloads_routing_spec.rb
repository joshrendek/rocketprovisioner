require "spec_helper"

describe PayloadsController do
  describe "routing" do

    it "routes to #index" do
      get("/payloads").should route_to("payloads#index")
    end

    it "routes to #new" do
      get("/payloads/new").should route_to("payloads#new")
    end

    it "routes to #show" do
      get("/payloads/1").should route_to("payloads#show", :id => "1")
    end

    it "routes to #edit" do
      get("/payloads/1/edit").should route_to("payloads#edit", :id => "1")
    end

    it "routes to #create" do
      post("/payloads").should route_to("payloads#create")
    end

    it "routes to #update" do
      put("/payloads/1").should route_to("payloads#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/payloads/1").should route_to("payloads#destroy", :id => "1")
    end

  end
end
