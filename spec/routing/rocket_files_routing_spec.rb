require "spec_helper"

describe RocketFilesController do
  describe "routing" do

    it "routes to #index" do
      get("/rocket_files").should route_to("rocket_files#index")
    end

    it "routes to #new" do
      get("/rocket_files/new").should route_to("rocket_files#new")
    end

    it "routes to #show" do
      get("/rocket_files/1").should route_to("rocket_files#show", :id => "1")
    end

    it "routes to #edit" do
      get("/rocket_files/1/edit").should route_to("rocket_files#edit", :id => "1")
    end

    it "routes to #create" do
      post("/rocket_files").should route_to("rocket_files#create")
    end

    it "routes to #update" do
      put("/rocket_files/1").should route_to("rocket_files#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/rocket_files/1").should route_to("rocket_files#destroy", :id => "1")
    end

  end
end
