require "spec_helper"

describe GyrosController do
  describe "routing" do

    it "routes to #index" do
      get("/gyros").should route_to("gyros#index")
    end

    it "routes to #new" do
      get("/gyros/new").should route_to("gyros#new")
    end

    it "routes to #show" do
      get("/gyros/1").should route_to("gyros#show", :id => "1")
    end

    it "routes to #edit" do
      get("/gyros/1/edit").should route_to("gyros#edit", :id => "1")
    end

    it "routes to #create" do
      post("/gyros").should route_to("gyros#create")
    end

    it "routes to #update" do
      put("/gyros/1").should route_to("gyros#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/gyros/1").should route_to("gyros#destroy", :id => "1")
    end

  end
end
