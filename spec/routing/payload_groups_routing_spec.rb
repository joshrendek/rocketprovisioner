require "spec_helper"

describe PayloadGroupsController do
  describe "routing" do

    it "routes to #index" do
      get("/payload_groups").should route_to("payload_groups#index")
    end

    it "routes to #new" do
      get("/payload_groups/new").should route_to("payload_groups#new")
    end

    it "routes to #show" do
      get("/payload_groups/1").should route_to("payload_groups#show", :id => "1")
    end

    it "routes to #edit" do
      get("/payload_groups/1/edit").should route_to("payload_groups#edit", :id => "1")
    end

    it "routes to #create" do
      post("/payload_groups").should route_to("payload_groups#create")
    end

    it "routes to #update" do
      put("/payload_groups/1").should route_to("payload_groups#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/payload_groups/1").should route_to("payload_groups#destroy", :id => "1")
    end

  end
end
