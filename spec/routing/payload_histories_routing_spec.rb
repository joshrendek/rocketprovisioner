require "spec_helper"

describe PayloadHistoriesController do
  describe "routing" do

    it "routes to #index" do
      get("/payload_histories").should route_to("payload_histories#index")
    end

    it "routes to #new" do
      get("/payload_histories/new").should route_to("payload_histories#new")
    end

    it "routes to #show" do
      get("/payload_histories/1").should route_to("payload_histories#show", :id => "1")
    end

    it "routes to #edit" do
      get("/payload_histories/1/edit").should route_to("payload_histories#edit", :id => "1")
    end

    it "routes to #create" do
      post("/payload_histories").should route_to("payload_histories#create")
    end

    it "routes to #update" do
      put("/payload_histories/1").should route_to("payload_histories#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/payload_histories/1").should route_to("payload_histories#destroy", :id => "1")
    end

  end
end
