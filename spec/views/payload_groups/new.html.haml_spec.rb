require 'spec_helper'

describe "payload_groups/new" do
  before(:each) do
    assign(:payload_group, stub_model(PayloadGroup,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new payload_group form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => payload_groups_path, :method => "post" do
      assert_select "input#payload_group_name", :name => "payload_group[name]"
    end
  end
end
