require 'spec_helper'

describe "payload_groups/show" do
  before(:each) do
    @payload_group = assign(:payload_group, stub_model(PayloadGroup,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
