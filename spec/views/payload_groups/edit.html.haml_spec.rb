require 'spec_helper'

describe "payload_groups/edit" do
  before(:each) do
    @payload_group = assign(:payload_group, stub_model(PayloadGroup,
      :name => "MyString"
    ))
  end

  it "renders the edit payload_group form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => payload_groups_path(@payload_group), :method => "post" do
      assert_select "input#payload_group_name", :name => "payload_group[name]"
    end
  end
end
