require 'spec_helper'

describe "payload_groups/index" do
  before(:each) do
    assign(:payload_groups, [
      stub_model(PayloadGroup,
        :name => "Name"
      ),
      stub_model(PayloadGroup,
        :name => "Name"
      )
    ])
  end

  it "renders a list of payload_groups" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
