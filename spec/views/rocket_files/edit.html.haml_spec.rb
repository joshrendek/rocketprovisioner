require 'spec_helper'

describe "rocket_files/edit" do
  before(:each) do
    @rocket_file = assign(:rocket_file, stub_model(RocketFile,
      :name => "MyString",
      :content => "MyText",
      :folder_id => 1
    ))
  end

  it "renders the edit rocket_file form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => rocket_files_path(@rocket_file), :method => "post" do
      assert_select "input#rocket_file_name", :name => "rocket_file[name]"
      assert_select "textarea#rocket_file_content", :name => "rocket_file[content]"
      assert_select "input#rocket_file_folder_id", :name => "rocket_file[folder_id]"
    end
  end
end
