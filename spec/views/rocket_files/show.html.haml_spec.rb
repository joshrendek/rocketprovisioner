require 'spec_helper'

describe "rocket_files/show" do
  before(:each) do
    @rocket_file = assign(:rocket_file, stub_model(RocketFile,
      :name => "Name",
      :content => "MyText",
      :folder_id => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/1/)
  end
end
