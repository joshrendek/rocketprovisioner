require 'spec_helper'

describe "rocket_files/index" do
  before(:each) do
    assign(:rocket_files, [
      stub_model(RocketFile,
        :name => "Name",
        :content => "MyText",
        :folder_id => 1
      ),
      stub_model(RocketFile,
        :name => "Name",
        :content => "MyText",
        :folder_id => 1
      )
    ])
  end

  it "renders a list of rocket_files" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
