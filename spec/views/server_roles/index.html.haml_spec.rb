require 'spec_helper'

describe "server_roles/index" do
  before(:each) do
    assign(:server_roles, [
      stub_model(ServerRole,
        :name => "Name"
      ),
      stub_model(ServerRole,
        :name => "Name"
      )
    ])
  end

  it "renders a list of server_roles" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
