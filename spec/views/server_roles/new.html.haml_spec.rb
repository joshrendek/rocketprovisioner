require 'spec_helper'

describe "server_roles/new" do
  before(:each) do
    assign(:server_role, stub_model(ServerRole,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new server_role form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => server_roles_path, :method => "post" do
      assert_select "input#server_role_name", :name => "server_role[name]"
    end
  end
end
