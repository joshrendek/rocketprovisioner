require 'spec_helper'

describe "server_roles/edit" do
  before(:each) do
    @server_role = assign(:server_role, stub_model(ServerRole,
      :name => "MyString"
    ))
  end

  it "renders the edit server_role form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => server_roles_path(@server_role), :method => "post" do
      assert_select "input#server_role_name", :name => "server_role[name]"
    end
  end
end
