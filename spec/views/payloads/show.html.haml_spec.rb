require 'spec_helper'

describe "payloads/show" do
  before(:each) do
    @payload = assign(:payload, stub_model(Payload,
      :name => "Name",
      :script => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
  end
end
