require 'spec_helper'

describe "payloads/index" do
  before(:each) do
    assign(:payloads, [
      stub_model(Payload,
        :name => "Name",
        :script => "MyText"
      ),
      stub_model(Payload,
        :name => "Name",
        :script => "MyText"
      )
    ])
  end

  it "renders a list of payloads" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
