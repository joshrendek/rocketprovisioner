require 'spec_helper'

describe "payloads/edit" do
  before(:each) do
    @payload = assign(:payload, stub_model(Payload,
      :name => "MyString",
      :script => "MyText"
    ))
  end

  it "renders the edit payload form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => payloads_path(@payload), :method => "post" do
      assert_select "input#payload_name", :name => "payload[name]"
      assert_select "textarea#payload_script", :name => "payload[script]"
    end
  end
end
