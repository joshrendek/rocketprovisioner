require 'spec_helper'

describe "payloads/new" do
  before(:each) do
    assign(:payload, stub_model(Payload,
      :name => "MyString",
      :script => "MyText"
    ).as_new_record)
  end

  it "renders new payload form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => payloads_path, :method => "post" do
      assert_select "input#payload_name", :name => "payload[name]"
      assert_select "textarea#payload_script", :name => "payload[script]"
    end
  end
end
