require 'spec_helper'

describe "payload_histories/new" do
  before(:each) do
    assign(:payload_history, stub_model(PayloadHistory,
      :server_id => 1,
      :payload_id => 1,
      :output => "MyText",
      :return_code => 1
    ).as_new_record)
  end

  it "renders new payload_history form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => payload_histories_path, :method => "post" do
      assert_select "input#payload_history_server_id", :name => "payload_history[server_id]"
      assert_select "input#payload_history_payload_id", :name => "payload_history[payload_id]"
      assert_select "textarea#payload_history_output", :name => "payload_history[output]"
      assert_select "input#payload_history_return_code", :name => "payload_history[return_code]"
    end
  end
end
