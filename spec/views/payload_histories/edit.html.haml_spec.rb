require 'spec_helper'

describe "payload_histories/edit" do
  before(:each) do
    @payload_history = assign(:payload_history, stub_model(PayloadHistory,
      :server_id => 1,
      :payload_id => 1,
      :output => "MyText",
      :return_code => 1
    ))
  end

  it "renders the edit payload_history form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => payload_histories_path(@payload_history), :method => "post" do
      assert_select "input#payload_history_server_id", :name => "payload_history[server_id]"
      assert_select "input#payload_history_payload_id", :name => "payload_history[payload_id]"
      assert_select "textarea#payload_history_output", :name => "payload_history[output]"
      assert_select "input#payload_history_return_code", :name => "payload_history[return_code]"
    end
  end
end
