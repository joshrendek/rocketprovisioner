require 'spec_helper'

describe "payload_histories/show" do
  before(:each) do
    @payload_history = assign(:payload_history, stub_model(PayloadHistory,
      :server_id => 1,
      :payload_id => 2,
      :output => "MyText",
      :return_code => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/MyText/)
    rendered.should match(/3/)
  end
end
