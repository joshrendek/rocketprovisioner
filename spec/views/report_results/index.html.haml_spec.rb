require 'spec_helper'

describe "report_results/index" do
  before(:each) do
    assign(:report_results, [
      stub_model(ReportResult,
        :report_id => 1,
        :server_id => 2,
        :result => "MyText"
      ),
      stub_model(ReportResult,
        :report_id => 1,
        :server_id => 2,
        :result => "MyText"
      )
    ])
  end

  it "renders a list of report_results" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
