require 'spec_helper'

describe "report_results/new" do
  before(:each) do
    assign(:report_result, stub_model(ReportResult,
      :report_id => 1,
      :server_id => 1,
      :result => "MyText"
    ).as_new_record)
  end

  it "renders new report_result form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => report_results_path, :method => "post" do
      assert_select "input#report_result_report_id", :name => "report_result[report_id]"
      assert_select "input#report_result_server_id", :name => "report_result[server_id]"
      assert_select "textarea#report_result_result", :name => "report_result[result]"
    end
  end
end
