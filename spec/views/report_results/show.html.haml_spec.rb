require 'spec_helper'

describe "report_results/show" do
  before(:each) do
    @report_result = assign(:report_result, stub_model(ReportResult,
      :report_id => 1,
      :server_id => 2,
      :result => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/MyText/)
  end
end
