require 'spec_helper'

describe "report_results/edit" do
  before(:each) do
    @report_result = assign(:report_result, stub_model(ReportResult,
      :report_id => 1,
      :server_id => 1,
      :result => "MyText"
    ))
  end

  it "renders the edit report_result form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => report_results_path(@report_result), :method => "post" do
      assert_select "input#report_result_report_id", :name => "report_result[report_id]"
      assert_select "input#report_result_server_id", :name => "report_result[server_id]"
      assert_select "textarea#report_result_result", :name => "report_result[result]"
    end
  end
end
