require 'spec_helper'

describe "server_groups/new" do
  before(:each) do
    assign(:server_group, stub_model(ServerGroup,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new server_group form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => server_groups_path, :method => "post" do
      assert_select "input#server_group_name", :name => "server_group[name]"
    end
  end
end
