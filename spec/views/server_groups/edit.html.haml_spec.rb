require 'spec_helper'

describe "server_groups/edit" do
  before(:each) do
    @server_group = assign(:server_group, stub_model(ServerGroup,
      :name => "MyString"
    ))
  end

  it "renders the edit server_group form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => server_groups_path(@server_group), :method => "post" do
      assert_select "input#server_group_name", :name => "server_group[name]"
    end
  end
end
