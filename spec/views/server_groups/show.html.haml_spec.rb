require 'spec_helper'

describe "server_groups/show" do
  before(:each) do
    @server_group = assign(:server_group, stub_model(ServerGroup,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
