require 'spec_helper'

describe "server_groups/index" do
  before(:each) do
    assign(:server_groups, [
      stub_model(ServerGroup,
        :name => "Name"
      ),
      stub_model(ServerGroup,
        :name => "Name"
      )
    ])
  end

  it "renders a list of server_groups" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
