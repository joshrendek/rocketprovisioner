require 'spec_helper'

describe "servers/show" do
  before(:each) do
    @server = assign(:server, stub_model(Server,
      :dns_name => "Dns Name",
      :ip_address => "Ip Address",
      :server_group_id => 1,
      :server_role_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Dns Name/)
    rendered.should match(/Ip Address/)
    rendered.should match(/1/)
    rendered.should match(/2/)
  end
end
