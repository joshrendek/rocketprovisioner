require 'spec_helper'

describe "servers/edit" do
  before(:each) do
    @server = assign(:server, stub_model(Server,
      :dns_name => "MyString",
      :ip_address => "MyString",
      :server_group_id => 1,
      :server_role_id => 1
    ))
  end

  it "renders the edit server form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => servers_path(@server), :method => "post" do
      assert_select "input#server_dns_name", :name => "server[dns_name]"
      assert_select "input#server_ip_address", :name => "server[ip_address]"
      assert_select "input#server_server_group_id", :name => "server[server_group_id]"
      assert_select "input#server_server_role_id", :name => "server[server_role_id]"
    end
  end
end
