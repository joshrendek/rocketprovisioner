require 'spec_helper'

describe "servers/index" do
  before(:each) do
    assign(:servers, [
      stub_model(Server,
        :dns_name => "Dns Name",
        :ip_address => "Ip Address",
        :server_group_id => 1,
        :server_role_id => 2
      ),
      stub_model(Server,
        :dns_name => "Dns Name",
        :ip_address => "Ip Address",
        :server_group_id => 1,
        :server_role_id => 2
      )
    ])
  end

  it "renders a list of servers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Dns Name".to_s, :count => 2
    assert_select "tr>td", :text => "Ip Address".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
