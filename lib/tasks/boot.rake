namespace :bootstrap do 
  task :setup => :environment do 
    Dir.foreach("#{Rails.root}/payload_bootstrap").each do |f| 
      if f.include? '.sh' 
        pl_name = f.split('.sh')[0]
        pl = Payload.find_or_create_by_name(pl_name)
        p "[Finding payload: #{pl_name}]"
        pl.script = IO.readlines("payload_bootstrap/#{f}").join
        pl.save!
      end
    end
  end
end
