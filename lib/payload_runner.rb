require 'net/ssh'
require 'net/sftp'
class PayloadRunner
  @queue = :payloads
  def self.perform(payload_id, server_id, action, ph_id, report_instance)
    server = Server.find(server_id)
    payload = Payload.find(payload_id)
    ph = PayloadHistory.find(ph_id)

    p "[Executing] #{payload.name} on #{server.dns_name}"
    if payload.payload_type == 'package' 
      if action == 'install'
        InstalledPayload.create(:server_id => server_id, :payload_id => payload_id)
        script = "apt-get -y install #{payload.script}"
      elsif action == 'remove'
        InstalledPayload.where(:server_id => server_id, :payload_id => payload_id).destroy_all
        script = "apt-get -y remove #{payload.script}"
      end
    else 
      script = payload.script
    end

    p "[Executing] #{script}"

    shell_name = "#{Time.now.to_i}.sh"
    `mkdir -p /tmp/rocketprovisioner/`
    filename = "/tmp/rocketprovisioner/#{shell_name}"
    p "[Writing #{filename}]"
    p "[Script: #{script}]"
    begin 
      File.open(filename, 'w') do |f| 
        script.split(/\n/).each do |sh| 
          p "[Writing: #{sh}]"
          f.write sh.gsub(/\r/, '') + "\n"
        end
      end
    rescue Exception => e
      p e
    end

    begin 
      Net::SFTP.start(server.ip_address, server.ssh_user) do |sftp|
        p "[Uploading #{filename}]"
        sftp.upload!(filename, "#{shell_name}")
        p "[Finished Uploading]"
      end
    rescue Exception => e
      p e
      p e.code
      p e.description
    end
    begin
      Net::SSH.start(server.ip_address, server.ssh_user) do |ssh|
        output = ""
        output = ssh.exec!("sudo sh #{shell_name}")
        ssh.exec!("rm -rf #{shell_name}")
        ph.output = output
        ph.finished = true 
        ph.save

        if report_instance.present?
          p "[Creating Report Result for instance #{report_instance}]"
          ri = ReportInstance.find(report_instance)
          rr = ReportResult.create(server_id: server_id, report_id: ri.report.id, 
                                   result: output, report_instance_id: report_instance)
          p "[ReportResult #{rr.id}]"
          # update report instance for this
        end
      end
    rescue Exception => e 
      p e
    ensure 
      Server.decrement_counter(:pending_payloads, server.id)
    end

    p "-- Finished"
  end
end
