RocketProvisioner::Application.routes.draw do

  get "report_instances/show"

  get "report_instances/index"

  resources :report_results

  resources :reports do 
    resources :report_instances
    member do 
      get 'run'
    end
  end


  resources :rocket_files

  resources :folders

  resources :payload_groups do 
    member do 
      get 'run/:object_type/:object_id', :to => "payload_groups#run", :as => "run"
      post 'add_payload'
      get 'payload/:payload_id/run', :as => 'run_payload', :to => "server_groups#run"
    end
  end


  resources :payloads

  resources :server_roles

  resources :payload_histories

  resources :servers do 
    resources :payload_histories
    member do 
      get 'payload/:payload_id/run', :as => 'run_payload', :to => "servers#run"
    end
  end

  root :to => "server_groups#index"

  resources :server_groups

  require 'resque'

  mount Resque::Server.new, :at => "/resque"

end
